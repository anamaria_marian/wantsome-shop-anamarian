package categoryshop;

import org.junit.Test;
import page_Objects.CategoryPage;
import page_Objects.HomeShopPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertEquals;

public class CategoryTests extends BaseTestClass {
    @Test
    public void checkMenuTitle(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage menCollection = homeShopPage.clickOnMenCollection();
        assertEquals("Men Collection", menCollection.getCategoryTitle());
    }
}
