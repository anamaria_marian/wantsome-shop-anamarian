package login;

import categories.RegressionTests;
import categories.SmokeTests;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import model.LoginModel;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import page_Objects.HomeShopPageFactory;
import page_Objects.LoginRegisterPageFactory;
import page_Objects.LostPasswordPageFactory;
import page_Objects.MyConnectPageFactory;
import utils.BaseTestClassWithResetBrowser;
import utils.LoadFromCSV;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static utils.Constants.*;

@RunWith(DataProviderRunner.class)

public class LoginTests extends BaseTestClassWithResetBrowser {

    @Category({SmokeTests.class})
    @Test
    public void loginWSuccess() {
        MyConnectPageFactory myConnectPageFactory = loginMethod(PASSWORD_GIVEN.get(0)).clickToLogin();

        assertTrue(myConnectPageFactory.myAccountMenuDisplayed());
    }

    @Category({SmokeTests.class})
    @Test
    public void logoutAfterLogin() {
        MyConnectPageFactory myConnectPageFactory = loginMethod(PASSWORD_GIVEN.get(0)).clickToLogin();
        LoginRegisterPageFactory loginPageFactory = myConnectPageFactory.clickToLogout();

        assertTrue(loginPageFactory.isDisplayedConnectMenu());
    }

    @Category({RegressionTests.class})
    @Test
    public void loginTextDisplayed() {
        MyConnectPageFactory myConnectPageFactory = loginMethod(PASSWORD_GIVEN.get(0)).clickToLogin();
        String username = EMAIL_INPUT.split("@")[0];

        assertEquals(myConnectPageFactory.contentDisplayedAtLogin(), "Hello " + username + " (not " + username + "?" + " Log out)");
    }

    @Category({SmokeTests.class})
    @Test
    public void shouldNotLogin() {
        LoginRegisterPageFactory loginRegisterPageFactory = loginMethod(PASSWORD_GIVEN.get(1));
        loginRegisterPageFactory.clickToLogin();

        assertEquals("ERROR: The password you entered for the email address 1611484697439aaa@test.com is incorrect. Lost your password?", loginRegisterPageFactory.getErrorMsg());

    }


    @Category({RegressionTests.class})
    //rularea scenariilor in care fiedl-urile required lipsesc
    @UseDataProvider("errorLoginModelProviderFromCsv")
    @Test
    public void NotLoginFromCsv(LoginModel loginModel) {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory loginPageFactory = homeShopPageFactory.goToLogin();
        loginPageFactory.fillLoginFields(loginModel);
        loginPageFactory.clickToLogin();

        assertTrue(loginPageFactory.isDisplayedErrorMsg());
        assertEquals(ERROR_MESSAGES_LOGIN.get(loginModel.getTestType()), loginPageFactory.getErrorMsg());
    }


    @Category({RegressionTests.class})
    @Test
    public void resetPasswordTest() {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory loginRegisterPageFactory = homeShopPageFactory.goToLogin();
        LostPasswordPageFactory lostPasswordPageFactory = loginRegisterPageFactory.forgetPassword();
        lostPasswordPageFactory.resetPassword(EMAIL_INPUT);

        assertEquals("Password reset email has been sent.", lostPasswordPageFactory.getResetMessage());
    }

    @Category({RegressionTests.class})
    @Test
    public void resetPasswordWithWrongEmailTest() {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory loginRegisterPageFactory = homeShopPageFactory.goToLogin();
        LostPasswordPageFactory lostPasswordPageFactory = loginRegisterPageFactory.forgetPassword();
        lostPasswordPageFactory.resetPassword("d@y.com");

        assertEquals("Invalid username or email.", lostPasswordPageFactory.getResetMessageWithWrongEmail());
    }

    @DataProvider
    public static Object[][] errorLoginModelProviderFromCsv() throws IOException {
        List<LoginModel> loginData = LoadFromCSV.getLogin();
        LoginModel[][] result = new LoginModel[loginData.size()][];
        for (int i = 0; i < loginData.size(); i++) {
            result[i] = new LoginModel[]{loginData.get(i)};
        }
        return result;
    }


    private LoginRegisterPageFactory loginMethod(String password) {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory loginPageFactory = homeShopPageFactory.goToLogin();
        loginPageFactory.fillLoginFields(new LoginModel("", EMAIL_INPUT, password));
        return loginPageFactory;
    }
}
