package cart;

import org.junit.Test;
import org.openqa.selenium.By;
import page_Objects.CartPage;
import page_Objects.CategoryPage;
import page_Objects.HomeShopPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertEquals;

public class CartTest extends BaseTestClass {

    @Test
    public void checkEmptyCart(){
        //click on a Cart tab
        driver.findElement(By.xpath("//a[text()='Cart']")).click();

        //check Cart page title
        assertEquals("CART", driver.findElement(By.className("entry-title")).getText());

        //check Cart page message
        assertEquals("Your cart is currently empty.", driver.findElement(By.className("cart-empty")).getText());
    }

    @Test
    public void checkEmptyCartPageObject(){
        HomeShopPage cart = new HomeShopPage(driver);
        CartPage cartPage = cart.clickOnCart();

        assertEquals("CART", cartPage.getCartTitle());
        assertEquals("Your cart is currently empty.", cartPage.getCartMessage());
    }

    @Test
    public void checkCartValue(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnCategory();
        CategoryPage categoryPage = homeShopPage.clickOnMenCollection();
        categoryPage.addToCart();
        CartPage cartPage = new CartPage(driver);
        assertEquals(1,cartPage.getCartValue());
    }
}
