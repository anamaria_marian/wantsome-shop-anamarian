package suites;

import comment.PostCommentTests;
import login.LoginTests;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import register.RegisterTests;

@RunWith(Suite.class)
@Suite.SuiteClasses({LoginTests.class, RegisterTests.class, PostCommentTests.class})
public class RegressionSuiteTest {
}
