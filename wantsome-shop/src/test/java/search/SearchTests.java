package search;

import org.junit.Test;
import page_Objects.HomeShopPage;
import page_Objects.SearchResultsPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertTrue;

public class SearchTests extends BaseTestClass {
    @Test
    public void searchTest(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        homeShopPage.clickOnSearch();
        SearchResultsPage searchPage = homeShopPage.typeSearch("watch");

        assertTrue(searchPage.checkResultContainsSearch("watch"));
    }

}
