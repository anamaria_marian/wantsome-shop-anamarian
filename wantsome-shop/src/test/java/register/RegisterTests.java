package register;

import categories.RegressionTests;
import categories.SmokeTests;
import com.gargoylesoftware.htmlunit.javascript.host.performance.Performance;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import model.RegisterModel;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import page_Objects.HomeShopPageFactory;
import page_Objects.LoginRegisterPageFactory;
import utils.BaseTestClass;
import utils.LoadFromCSV;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static utils.Constants.*;

@RunWith(DataProviderRunner.class)
public class RegisterTests extends BaseTestClass {


    //scenariul in care utilizatorul se inregisteaza cu succes
    @Category({Performance.class, SmokeTests.class, RegressionTests.class})
    @Test
    public void registerFromCsv() throws IOException {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory registerPageFactory = homeShopPageFactory.goToRegister();
        List<RegisterModel> regTest = LoadFromCSV.getRegister();
        registerPageFactory.fillFormRegister(regTest.get(0));
        registerPageFactory.register();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        assertEquals(WANTSOME_SHOP_HOME, driver.getCurrentUrl());
    }


    @DataProvider
    public static Object[][] registerModelProvider() throws IOException {
        List<RegisterModel> registerData = LoadFromCSV.getRegister();
        RegisterModel[][] result = new RegisterModel[FIRST_LINES_FROM_CSV][];
        for (int i = 0; i < FIRST_LINES_FROM_CSV; i++) {
            result[i] = new RegisterModel[]{registerData.get(i)};
        }
        return result;
    }

    @Category({Performance.class, SmokeTests.class, RegressionTests.class})
    @UseDataProvider("registerModelProvider")
    @Test
    public void registerFromCsvFirst2Lines(RegisterModel registerModel) {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory registerPageFactory = homeShopPageFactory.goToRegister();

        registerPageFactory.fillFormRegister(registerModel);
        switch (registerModel.getTestType()) {
            case "testOk":
                registerPageFactory.register();
                assertEquals(WANTSOME_SHOP_HOME, driver.getCurrentUrl());
                break;
            case "testWrong":
                assertFalse(registerPageFactory.isRegisterButtonEnabled());
        }
    }


    @DataProvider
    public static Object[][] errorRegisterModelProviderFromCsv() throws IOException {
        List<RegisterModel> registerData = LoadFromCSV.getRegister();
        RegisterModel[][] result = new RegisterModel[registerData.size() - FIRST_LINES_FROM_CSV][];
        for (int i = FIRST_LINES_FROM_CSV; i < registerData.size(); i++) {
            result[i - FIRST_LINES_FROM_CSV] = new RegisterModel[]{registerData.get(i)};
        }
        return result;
    }

    @Category({Performance.class, SmokeTests.class, RegressionTests.class})
    //rularea scenariilor in care fiedl-urile required lipsesc
    @UseDataProvider("errorRegisterModelProviderFromCsv")
    @Test
    public void NotRegisterFromCsv(RegisterModel registerModel) {
        HomeShopPageFactory homeShopPageFactory = new HomeShopPageFactory(driver);
        LoginRegisterPageFactory registerPageFactory = homeShopPageFactory.goToRegister();
        registerPageFactory.fillFormRegister(registerModel);
        registerPageFactory.register();

        assertTrue(registerPageFactory.isDisplayedErrorMsg());
        assertEquals(ERROR_MESSAGES_REGISTER.get(registerModel.getTestType()), registerPageFactory.getErrorMsg());
    }

}
