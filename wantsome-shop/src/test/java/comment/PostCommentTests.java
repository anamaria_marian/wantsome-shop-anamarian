package comment;

import categories.RegressionTests;
import categories.SmokeTests;
import model.CommentUsers;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import page_Objects.CommentPage;
import page_Objects.HomeShopPage;
import utils.BaseTestClass;

import static org.junit.Assert.assertTrue;

public class PostCommentTests extends BaseTestClass {
    public PostCommentTests() {
    }

    @Category({SmokeTests.class, RegressionTests.class})
    @Test
    public void postACommentTest() {
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CommentPage commentPage = homeShopPage.clickOnBlog().clickOnWatch();
        String commentText = "I love this product" + System.currentTimeMillis();
        CommentUsers commUsers = CommentUsers.builder()
                .setComment(commentText)
                .setName("WA6" + System.currentTimeMillis())
                .setEmail("bla" + System.currentTimeMillis() + "@test.test")
                .build();
        commentPage.leaveAComment(commUsers);

        assertTrue(commentPage.isMyCommentPosted(commentText));
    }

    @Category({SmokeTests.class, RegressionTests.class})
    @Test
    public void postCommError(){
        HomeShopPage homeShopPage = new HomeShopPage(driver);
        CommentPage commentPageWError = homeShopPage.clickOnBlog().clickOnWatch();
        CommentUsers commUsers = CommentUsers.builder()
                .setComment("Test coment BIS")
                .setName("User" + System.currentTimeMillis())
                .build();
        commentPageWError.errorComm(commUsers);

        assertTrue(driver.getPageSource().contains("Comment Submission Failure"));
    }
}
