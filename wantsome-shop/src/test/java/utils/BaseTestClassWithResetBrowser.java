package utils;

import org.apache.commons.lang3.SystemUtils;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static utils.Constants.WANTSOME_SHOP_HOME;

public class BaseTestClassWithResetBrowser {
    public WebDriver driver;

    @Before
    public void setUpTest() {
        driver = BrowserFactory.getBrowser("chrome");
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        driver.get(WANTSOME_SHOP_HOME);
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}

