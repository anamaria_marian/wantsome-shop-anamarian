package utils;

import com.google.common.base.Strings;
import model.CommentUsers;
import model.LoginModel;
import model.RegisterModel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static utils.Constants.*;

//am folosit abstract(nu vreau sa fac o instanta de clasa asta) si am creat clasa in utils pt a folosi metodele definite si la alte teste
/*
 *    abstract - aceasta clasa va fi folosita ca si helper si abstract este folosit ca o constrangere
 *               pentru a nu o putea instatia
 *
 * */
public abstract class LoadFromCSV {

    public static List<CommentUsers> getComments() throws IOException {
        List<CommentUsers> comments = new ArrayList<>();
        for (String lineComm : readAllLinesFromFile(COMMENTS_CSV_FILE)) {
            String[] fields = lineComm.split(",", -1);
            comments.add(CommentUsers.builder()
                    .setComment(fields[0])
                    .setName(fields[1])
                    .setEmail(fields[2])
                    .setWebsite(fields[3])
                    .build());
        }
        return comments;
    }


    public static List<RegisterModel> getRegister() throws IOException {
        List<RegisterModel> register = new ArrayList<>();
        for (String registerLine : readAllLinesFromFile(REGISTER_CSV_FILE)) {
            String[] fields = registerLine.split(",", -1);
            String email = Strings.isNullOrEmpty(fields[1]) ? fields[1] : System.currentTimeMillis() + fields[1];
            register.add(new RegisterModel(fields[0], email, fields[2], fields[3], fields[4], fields[5]));
        }
        return register;
    }

    public static List<LoginModel> getLogin() throws IOException {
        List<LoginModel> login = new ArrayList<>();
        for (String loginLine : readAllLinesFromFile(LOGIN_CSV_FILE)) {
            String[] fields = loginLine.split(",", -1);
            login.add(new LoginModel(fields[0], fields[1], fields[2]));
        }
        return login;
    }


    private static List<String> readAllLinesFromFile(String filename) throws IOException {
        List<String> allLines = new ArrayList<>();
        File file = getFileFromResources(filename);

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)) {

            String line = br.readLine();
            while (line != null) { // (line=br.readline())
                allLines.add(line);
                line = br.readLine();
            }
        }

        return allLines;
    }

    private static File getFileFromResources(String fileName) {
        ClassLoader classLoader = LoadFromCSV.class.getClassLoader();
        URL resource = classLoader.getResource(fileName);
        File file = null;
        if (resource == null) {
            throw new IllegalArgumentException("File not Found!");
        } else {
            file = new File(resource.getFile());
        }
        return file;
    }


}
