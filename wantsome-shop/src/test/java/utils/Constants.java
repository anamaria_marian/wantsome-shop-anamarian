package utils;

import org.apache.groovy.util.Maps;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public abstract class Constants {
    public static final String COMMENTS_CSV_FILE = "comments.csv";
    public static final String REGISTER_CSV_FILE = "register.csv";
    public static final String LOGIN_CSV_FILE = "login.csv";

    //register
    public static final int FIRST_LINES_FROM_CSV = 2;
    public static final Map<String, String> ERROR_MESSAGES_REGISTER = Maps.of(
            "test1Wrong", "Error: Please provide a valid email address.",
            "test2Wrong", "Error: Please enter an account password.",
            "test3Wrong", "Error: First name is required!",
            "test4Wrong", "Error: Last name is required!",
            "test5Wrong", "Error: Phone number is required!");

    //login
    public static final Map<String, String> ERROR_MESSAGES_LOGIN = Maps.of(
            "loginPasswordWrong", "ERROR: The password you entered for the email address 1611484697439aaa@test.com is incorrect. Lost your password?",
            "loginEmailWrong", "Unknown email address. Check again or try your username.",
            "loginWrong", "Unknown email address. Check again or try your username.");

    public static final List<String> PASSWORD_GIVEN = Arrays.asList("Abc!12345678@", "'OR' '='");
    public static final String EMAIL_INPUT = "1611484697439aaa@test.com";

    public static final String WANTSOME_SHOP_HOME = "https://testare-automata.practica.tech/shop/";
}
