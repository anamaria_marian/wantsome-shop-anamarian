package utils;

import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import static utils.Constants.WANTSOME_SHOP_HOME;

public class BaseTestClass {
    public static WebDriver driver;

    @BeforeClass
    public static void setUpTest() {
        driver = BrowserFactory.getBrowser("chrome");
        if (SystemUtils.IS_OS_WINDOWS) {
            driver.manage().window().maximize();
        }
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
    }

    @Before
    public void setUp() {
        driver.get(WANTSOME_SHOP_HOME);
    }

    @AfterClass
    public static void tearDown() {
        driver.quit();
    }

}