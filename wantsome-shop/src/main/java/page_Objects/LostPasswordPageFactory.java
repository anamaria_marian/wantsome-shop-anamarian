package page_Objects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LostPasswordPageFactory {
    private WebDriver driver;

    @FindBy(id = "user_login")
    @CacheLookup
    private WebElement getEmailRecovery;

    @FindBy(className = "woocommerce-message")
    @CacheLookup
    private WebElement resetMessageDisplayed;

    @FindBy(className = "woocommerce-error")
    @CacheLookup
    private WebElement resetMessageDisplayedWithWrongEmail;


    public LostPasswordPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void resetPassword(String email) {
        getEmailRecovery.sendKeys(email);
        getEmailRecovery.sendKeys(Keys.ENTER);
    }

    public String getResetMessage() {
        return resetMessageDisplayed.getText();
    }

    public String getResetMessageWithWrongEmail() {
        return resetMessageDisplayedWithWrongEmail.getText();
    }
}
