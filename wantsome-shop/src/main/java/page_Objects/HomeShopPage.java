package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class HomeShopPage {
    //adaugam un obiect de tip driver
    WebDriver driver;

    //trebuie generat un constructor
    public HomeShopPage(WebDriver driver) {
        this.driver = driver;
    }

    //identificam un locator pt butonul de CART si-l facem private
    private final By CART_BUTTON = By.xpath(("//a[text()='Cart']"));
    private final By CATEGORY_BUTTON = By.className("category-toggle");
    private final By MEN_COLLECTION = By.id("menu-item-509");
    private final By SEARCH_ICON = By.className("search-icon");
    private final By SEARCH_FIELD = By.className("search-field");


    // Final Project
    private final By BLOG_ICON = By.id("menu-item-563");


    public CartPage clickOnCart() {
        driver.findElement(CART_BUTTON).click();
        return new CartPage(driver);
    }


    // click pe Categorie si apoi pe articole barbatesti si sa verifici titlul ca este Men Collection
    public void clickOnCategory() {
        driver.findElement(CATEGORY_BUTTON).click();
    }

    public CategoryPage clickOnMenCollection() {
        driver.findElement(MEN_COLLECTION).click();
        return new CategoryPage(driver);
    }


    ////-- pt search
    public void clickOnSearch() {
        driver.findElement(SEARCH_ICON).click();
    }

    public SearchResultsPage typeSearch(String searchInput) {
        driver.findElement(SEARCH_FIELD).sendKeys(searchInput);
        driver.findElement(SEARCH_FIELD).sendKeys(Keys.ENTER);
        return new SearchResultsPage(driver);
    }

    //Final Project

    public BlogPage clickOnBlog() {
        driver.findElement(BLOG_ICON).click();
        return new BlogPage(driver);
    }


}
