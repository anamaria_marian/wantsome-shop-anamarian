package page_Objects;

import model.LoginModel;
import model.RegisterModel;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//am creat PageObject-ul care integreaza atat partea de Register cat si partea de Login
//m-am folosit de PageFactory
// impreuna cu cele doua modele(Register si Login) am accesat field-urile de pe pagina.

public class LoginRegisterPageFactory {
    private WebDriver driver;

    public LoginRegisterPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //Register fields
    @FindBy(id = "reg_email")
    @CacheLookup
    private WebElement emailAddress;

    @FindBy(id = "reg_password")
    @CacheLookup
    private WebElement passwordR;

    @FindBy(id = "registration_field_1")
    @CacheLookup
    private WebElement firstName;

    @FindBy(id = "registration_field_2")
    @CacheLookup
    private WebElement lastName;

    @FindBy(id = "registration_field_3")
    @CacheLookup
    private WebElement phoneNumber;

    @FindBy(className = "woocommerce-form-register__submit")
    @CacheLookup
    private WebElement registerBtn;

    @FindBy(className = "woocommerce-error")
    @CacheLookup
    private WebElement errorMsg;

    //Login Fields
    @FindBy(id = "username")
    @CacheLookup
    private WebElement usernameL;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement passwordL;

    @FindBy(className = "woocommerce-form-login__submit")
    @CacheLookup
    private WebElement loginBtn;

    @FindBy(id = "menu-item-566")
    @CacheLookup
    private WebElement menuOptionConnect;

    @FindBy(linkText = "Lost your password?")
    @CacheLookup
    private WebElement lostPasswordButton;


    public void fillFormRegister(RegisterModel registerModel) {
        emailAddress.sendKeys(registerModel.getEmailAddress());
        //am folosit .sendKeys(Keys.chord()) pt a simula typing-ul efectiv de parola
        //.sendKeys(registerModel.getPassword()) doar imi seta parola in acel field
        passwordR.sendKeys(Keys.chord(registerModel.getPassword()));
        firstName.sendKeys(registerModel.getFirstName());
        lastName.sendKeys(registerModel.getLastName());
        phoneNumber.sendKeys(registerModel.getPhoneNumber());
    }

    public void register() {
        registerBtn.click();
    }

    public Boolean isRegisterButtonEnabled() {
        return registerBtn.isEnabled();
    }

    public Boolean isDisplayedErrorMsg() {
        return errorMsg.isDisplayed();
    }

    public String getErrorMsg() {
        return errorMsg.getText();
    }

    public void fillLoginFields(LoginModel loginModel) {
        usernameL.sendKeys(loginModel.getUsername());
        passwordL.sendKeys(loginModel.getPassword());
    }

    public MyConnectPageFactory clickToLogin() {
        loginBtn.click();
        return new MyConnectPageFactory(driver);
    }

    public boolean isDisplayedConnectMenu() {
        return menuOptionConnect.isDisplayed();
    }

    public LostPasswordPageFactory forgetPassword() {
        lostPasswordButton.click();
        return new LostPasswordPageFactory(driver);
    }

}
