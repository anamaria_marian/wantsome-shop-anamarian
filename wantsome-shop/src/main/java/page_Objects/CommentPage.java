package page_Objects;

import model.CommentUsers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class CommentPage {
    WebDriver driver;
    private final By NAME_FIELD = By.id("author");
    private final By EMAIL_FIELD = By.id("email");
    private final By COMMENT_FIELD = By.id("comment");
    private final By WEBSITE_FIELD = By.id("url");
    private final By POST_COMM = By.id("submit");

    public CommentPage(WebDriver driver) {
        this.driver = driver;
    }

    public void leaveAComment(CommentUsers commUsers) {
        driver.findElement(COMMENT_FIELD).sendKeys(commUsers.getComment());
        driver.findElement(NAME_FIELD).sendKeys(commUsers.getName());
        driver.findElement(EMAIL_FIELD).sendKeys(commUsers.getEmail());
        driver.findElement(WEBSITE_FIELD).sendKeys(commUsers.getWebsite());
        driver.findElement(POST_COMM).click();
    }

    public void errorComm(CommentUsers commUsers) {
        driver.findElement(COMMENT_FIELD).sendKeys(commUsers.getComment());
        driver.findElement(NAME_FIELD).sendKeys(commUsers.getName());
        driver.findElement(WEBSITE_FIELD).sendKeys(commUsers.getWebsite());
        driver.findElement(POST_COMM).click();
    }

    public boolean isMyCommentPosted(String commentText) {
        return driver.findElement(By.xpath("//*[contains(text(),'" + commentText + "')]")) != null;
    }
}
