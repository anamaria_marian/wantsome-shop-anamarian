package page_Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomeShopPageFactory {
    WebDriver driver;
    @FindBy(id ="menu-item-566")
    @CacheLookup
    private WebElement connectBtn;


    public HomeShopPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginRegisterPageFactory goToRegister(){
        connectBtn.click();
        return new LoginRegisterPageFactory(driver);
    }


    public LoginRegisterPageFactory goToLogin(){
        connectBtn.click();
        return new LoginRegisterPageFactory(driver);
    }




}
