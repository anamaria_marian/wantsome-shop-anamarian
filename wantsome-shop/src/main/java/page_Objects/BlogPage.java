package page_Objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BlogPage {
    WebDriver driver;
    private final By WATCH_POST = By.linkText("Gold Watch");

    public BlogPage(WebDriver driver) {
        this.driver = driver;
    }

    public CommentPage clickOnWatch() {
        driver.findElement(WATCH_POST).click();
        return new CommentPage(driver);
    }
}
