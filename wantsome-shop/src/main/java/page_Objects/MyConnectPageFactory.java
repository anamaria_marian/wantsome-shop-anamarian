package page_Objects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyConnectPageFactory {
    private WebDriver driver;
    @FindBy(linkText = "Log out")
    @CacheLookup
    private WebElement logoutBtn;

    @FindBy(id = "menu-item-567")
    @CacheLookup
    private WebElement menuOptionMyAccount;

    @FindBy(css = "[class=\"woocommerce-MyAccount-content\"] p:nth-child(2)")
    @CacheLookup
    private WebElement contentDisplayed;

    public MyConnectPageFactory(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public LoginRegisterPageFactory clickToLogout() {
        logoutBtn.click();
        return new LoginRegisterPageFactory(driver);
    }

    public boolean myAccountMenuDisplayed() {
        return menuOptionMyAccount.isDisplayed();
    }

    public String contentDisplayedAtLogin() {
        return contentDisplayed.getText();
    }

}
