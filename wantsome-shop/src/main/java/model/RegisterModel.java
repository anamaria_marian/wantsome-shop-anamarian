package model;

public class RegisterModel {
    private final String testType;
    private final String emailAddress;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String phoneNumber;

    public RegisterModel(String testType, String emailAddress, String password, String firstName, String lastName, String phoneNumber) {
        this.emailAddress = emailAddress;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.testType = testType;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTestType() {
        return testType;
    }

}
