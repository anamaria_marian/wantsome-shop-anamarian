package model;

public class CommentUsers {
    private String comment;
    private String name;
    private String email;
    private String website;

    public CommentUsers(String comment, String name, String email, String website) {
        this.comment = comment;
        this.name = name;
        this.email = email;
        this.website = website;
    }


    public String getComment() {
        return comment;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getWebsite() {
        return website;
    }

    public static CommUsersBuilder builder() {
        return new CommUsersBuilder();
    }

    public static class CommUsersBuilder {
        private String comment;
        private String name;
        private String email;
        private String website;


        public CommUsersBuilder setComment(String comment) {
            this.comment = comment;
            return this;
        }

        public CommUsersBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public CommUsersBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public CommUsersBuilder setWebsite(String website) {
            this.website = website;
            return this;
        }

        public CommentUsers build() {
            return new CommentUsers(comment, name, email, website);
        }
    }
}
