package model;

public class LoginModel {
    private final String testType;
    private final String username;
    private final String password;

    public LoginModel(String testType, String username, String password) {
        this.username = username;
        this.password = password;
        this.testType = testType;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getTestType() {
        return testType;
    }
}
